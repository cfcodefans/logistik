class HandlerNotFoundException(Exception):
    pass


class HandlerExistsException(Exception):
    pass


class ParseException(Exception):
    pass
